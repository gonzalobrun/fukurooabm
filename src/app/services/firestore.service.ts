import { Injectable } from '@angular/core';

import { AngularFirestore, AngularFirestoreDocument } from 'angularfire2/firestore';
import { Observable } from 'rxjs';
import { Member } from '../interfaces/member.interface';

@Injectable({
  providedIn: 'root'
})
export class FirestoreService {

  constructor(private firestore: AngularFirestore) { }

  public getAllMembers() {
    return this.firestore.collection('members').snapshotChanges();
  }

  public createMember(member: Member) {
    return this.firestore.collection('members').add(member);
  }

  public getMember(memberId: string) {
    return this.firestore.collection('members').doc(memberId).snapshotChanges();
  }

  public updateMember(member: Member) {
    return this.firestore.collection('members').doc(member.id).set(member);
  }

  public deleteMember(member: Member) {
    return this.firestore.collection('members').doc(member.id).delete();
  }

}
