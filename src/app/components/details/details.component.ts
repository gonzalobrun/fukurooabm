import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { FirestoreService } from '../../services/firestore.service';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Member } from '../../interfaces/member.interface';

@Component({
  selector: 'app-details',
  templateUrl: './details.component.html',
  styleUrls: ['./details.component.scss']
})
export class DetailsComponent implements OnInit, OnDestroy {

  private memberId: any = null;
  private params: any;
  public member: any;
  public memberForm: FormGroup;
  public showForm = false;
  private formSubscription: any;
  private isEditing = false;

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private firestoreService: FirestoreService) { }

  ngOnInit() {
    this.params = this.route.params.subscribe(params => {
      this.memberId = params['id'];
    });
    this.getMember();
    this.buildForm();
    this.memberForm.disable();
  }

  public getMember() {
    this.formSubscription = this.firestoreService.getMember(this.memberId).subscribe((member) => {
      this.member = member.payload.data();
      this.member.id = member.payload.id;
      this.setUpForm();
    });
  }

  public buildForm(): void {
    this.memberForm = new FormGroup({
      'name': new FormControl('', [Validators.required, Validators.minLength(3)]),
      'lastName': new FormControl('', [Validators.required]),
      'adress': new FormControl('', [Validators.required]),
      'phone': new FormControl('', [Validators.required]),
      'email': new FormControl('', [Validators.required]),
    });
  }
  private setUpForm() {
    this.memberForm.controls.name.setValue(this.member.data.name);
    this.memberForm.controls.lastName.setValue(this.member.data.lastName);
    this.memberForm.controls.adress.setValue(this.member.data.adress);
    this.memberForm.controls.phone.setValue(this.member.data.phone);
    this.memberForm.controls.email.setValue(this.member.data.email);
  }

  public edit(): void {
    if (!this.isEditing) {
      this.memberForm.enable();
    } else {
      this.memberForm.disable();
    }
    this.isEditing = !this.isEditing;
  }

  public save(): void {
    console.log('saved');
    Object.assign(this.member.data, this.memberForm.value);
    this.firestoreService.updateMember(this.member);
  }

  public delete(): void {
    this.firestoreService.deleteMember(this.member);
    this.router.navigate(['/']);
  }

  ngOnDestroy() {
    this.params.unsubscribe();
    this.formSubscription.unsubscribe();
  }
}
