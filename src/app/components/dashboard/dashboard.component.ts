import { Component, OnInit } from '@angular/core';
import { FirestoreService } from '../../services/firestore.service';
import { Member } from '../../interfaces/member.interface';


@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit {

  public members: Array<any> = [];
  public member: Member = {
    data: {
      name: 'name',
      lastName: 'last',
      adress: 'adress',
      phone: 'phone',
      email: 'mail'
    }
  };

  constructor(private firestoreService: FirestoreService) { }

  ngOnInit() {
    this.firestoreService.getAllMembers().subscribe((members) => {
      members.forEach((member: any) => {
        this.members.push({
          id: member.payload.doc.id,
          data: member.payload.doc.data().data,
        });
      });
      console.log(this.members);
    });
  }

}
