import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { FirestoreService } from '../../services/firestore.service';
import { Member } from '../../interfaces/member.interface';
import { Router } from '@angular/router';

@Component({
  selector: 'app-new-member',
  templateUrl: './new-member.component.html',
  styleUrls: ['./new-member.component.scss']
})
export class NewMemberComponent implements OnInit {

  public newMemberForm: FormGroup;
  public member: Member = {
    data: {
      name: null,
      lastName: null,
      adress: null,
      phone: null,
      email: null
    }
  };

  constructor(
    private route: Router,
    private firestoreService: FirestoreService
    ) { }

  ngOnInit() {
    this.buildForm();
  }

  public buildForm(): void {
    this.newMemberForm = new FormGroup({
      'name': new FormControl('', [Validators.required, Validators.minLength(3)]),
      'lastName': new FormControl('', [Validators.required]),
      'adress': new FormControl('', [Validators.required]),
      'phone': new FormControl('', [Validators.required]),
      'email': new FormControl('', [Validators.required]),
    });
  }

  public create(): void {
    Object.assign(this.member.data, this.newMemberForm.value);
    this.firestoreService.createMember(this.member);
    this.route.navigate(['/']);
  }
}
