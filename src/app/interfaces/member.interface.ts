export interface Member {
    id?: string;
    data: {
        name: string;
        lastName: string;
        adress: string;
        phone: string;
        email: string;
    };
}
