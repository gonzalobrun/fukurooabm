import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { DashboardComponent } from './components/dashboard/dashboard.component';

import { AngularFireModule } from 'angularfire2';
import { environment } from '../environments/environment';
import { FirestoreService } from './services/firestore.service';
import { AngularFirestore } from 'angularfire2/firestore';
import { DetailsComponent } from './components/details/details.component';
import { NewMemberComponent } from './components/new-member/new-member.component';
import { importType } from '@angular/compiler/src/output/output_ast';

@NgModule({
  declarations: [
    AppComponent,
    DashboardComponent,
    DetailsComponent,
    NewMemberComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    ReactiveFormsModule,
    AngularFireModule.initializeApp(environment.firebase)
  ],
  providers: [AngularFirestore, FirestoreService],
  bootstrap: [AppComponent]
})
export class AppModule { }
